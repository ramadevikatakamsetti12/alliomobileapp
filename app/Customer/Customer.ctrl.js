import { default as Customers } from './Customer.model';
import { Obj } from './Customer.class';
import validator from 'validator';
import otpgenerator from 'otp-generator';
import jwt from 'jsonwebtoken';
import { dbConfig } from '../../config/env/development';
import Cryptr from 'cryptr';
const SendOtp = require('sendotp');
const sendOtp = new SendOtp('261493AxznjpI35P5c59a314');

var cryptr = new Cryptr('myTotalySecretKeyStepVTech');
let customerCtrl = {};

customerCtrl.userRegistration = (req, res, next) => {
    let payload = new Obj(req.body);
    console.log(payload);
    var userData = {
        first_Name: payload.first_Name,
        last_Name: payload.last_Name,
        email: payload.email,
        address: payload.address,
        mobile: payload.mobile,
        type: payload.type,
        role: payload.role
    };
    try {
        Customers.find({ mobile: payload.mobile }, (err, result) => {
            if (err) {
                res.json({
                    code: 400,
                    message: err
                })
                console.log(err)
            } else {
                if (result && result.length != 0) {
                    res.json({
                        code: 400,
                        message: 'Customer already exists, Try with other Moblie Number'
                    })
                } else {

                    Customers.insertMany(userData).then((result) => {
                        req.body.mobile = userData.mobile;
                        console.log('mobile1:', req.body.mobile)
                        next();
                    }).catch((errr) => {
                        res.json({
                            code: 400,
                            message: errr
                        })
                        console.log(errr);
                    })
                }
            }
        })
    } catch (error) {
        res.json({
            code: 400,
            message: error
        })
        console.log(error)
    }

}


customerCtrl.GenerateOtpForUSerRegistration = (req, res, next) => {
    let payload = new Obj(req.body);
    console.log(payload);
    let loginData = {
        mobile: payload.mobile
    }
    try {
        let otp = otpgenerator.generate(6, { digits: true, alphabets: false, specialChars: false, upperCase: false });
        req.body.otp = otp;
        req.body.mobile = payload.mobile;
        console.log('mobile:', req.body.mobile, 'otp:', req.body.otp)

        sendOtp.send(req.body.mobile, "611331", req.body.otp, function (error, data) {
            if (error) {
                console.log(error);
            } else {
                //  sendOtp.setOtpExpiry('1');
                next();
            }
        });
    } catch (err) {
        console.log(err);
        let err = err
        res.json({
            code: 400,
            message: err
        })
    }
}

customerCtrl.updateOtpForUserRegistration = (req, res, next) => {
    let payload = new Obj(req.body);
    console.log("in generate otp block");
    let loginData = {
        mobile: payload.mobile
    }
    try {
        Customers.findOne(loginData, (err, result) => {
            if (err) {
                res.json({
                    code: 400,
                    message: err
                })
            } else {
                if (result) {

                    let otp = otpgenerator.generate(6, { digits: true, alphabets: false, specialChars: false, upperCase: false });
                    req.body.otp = otp;
                    req.body.mobile = payload.mobile;
                    console.log('mobile2:', req.body.mobile, 'otp1:', req.body.otp)

                    sendOtp.send(req.body.mobile, "611331", req.body.otp, function (error, data) {
                        if (error) {
                            console.log(error);
                        } else {
                            console.log('mobile3:', req.body.mobile, 'otp2:', req.body.otp)
                            next();
                        }
                    });
                }
                else {
                    res.json({
                        code: 204,
                        message: 'Try With Correct Mobile Number'
                    })
                }
            }
        })
    } catch (err) {
        console.log(err);
        let err = err
        res.json({
            code: 400,
            message: err
        })
    }
}
customerCtrl.updateOtpForLogin = (req, res, next) => {
    let payload = new Obj(req.body);
    console.log(payload);
    let loginData = {
        mobile: payload.mobile
    }
    try {
        Customers.findOne(loginData, (err, result) => {
            if (err) {
                res.json({
                    code: 400,
                    message: err
                })
            } else {
                if (result) {
                    if (!result.is_User_Registered) {
                        res.json({
                            code: 204,
                            message: 'User Not Registered Sucessfully'
                        })
                    }
                    else {
                        let otp = otpgenerator.generate(6, { digits: true, alphabets: false, specialChars: false, upperCase: false });
                        req.body.otp = otp;
                        req.body.mobile = payload.mobile;
                        console.log('mobile:', req.body.mobile, 'otp:', req.body.otp)

                        sendOtp.send(req.body.mobile, "611331", req.body.otp, function (error, data) {
                            if (error) {
                                console.log(error);
                            } else {
                                next();
                            }
                        });
                    }
                }
                else {
                    res.json({
                        code: 204,
                        message: 'Try With Correct Mobile Number'
                    })
                }
            }
        })
    } catch (err) {
        console.log(err);
        let err = err
        res.json({
            code: 400,
            message: err
        })
    }
}

customerCtrl.saveOtp = (req, res) => {
    let payload = new Obj(req.body);
    let loginData = {
        mobile: payload.mobile
    }
    let otpUpdate = {
        login_Otp: payload.otp
    }
    try {
        Customers.updateOne(loginData, { $set: otpUpdate }).then((result) => {
            console.log(result.n);
            if (result && result.n == 1) {
                res.json({
                    code: 200,
                    message: 'OTP sent to your Mobile'
                })
            }
        }).catch((err) => {
            console.log(err);
            res.json({
                code: 400,
                message: err
            })
        })
    } catch (err) {
        console.log(err);
        let err = err
        res.json({
            code: 400,
            message: err
        })
    }
}

customerCtrl.userLogin = (req, res, next) => {
    let payload = new Obj(req.body);
    let loginData = {
        mobile: payload.mobile
    }
    console.log(loginData);
    try {
        Customers.findOne(loginData, (err, result) => {
            if (err) {
                res.json({
                    code: 400,
                    message: err
                })
            } else {
                if (result) {
                    if (result.mobile == payload.mobile) {
                        payload.customer_data = result;
                        req.body.mobile = result.mobile;
                        next();


                    } else {
                        res.json({
                            code: 204,
                            message: 'Invalid User'
                        })
                    }
                }

            }
        })
    } catch (err) {
        console.log(err);
        let err = err
        res.json({
            code: 400,
            message: err
        })
    }
}

customerCtrl.verificationOfRegisteredUserold = (req, res) => {

    let payload = new Obj(req.body);
    console.log(payload);
    let loginData = {
        mobile: payload.mobile
    }
    try {
        Customers.findOne(loginData, (err, result) => {
            if (err) {
                res.json({
                    code: 400,
                    message: err
                })
            } else {
                if (result) {
                    console.log(result);
                    if (result.mobile == payload.mobile && result.login_Otp == payload.login_Otp) {
                        sendOtp.verify(payload.mobile, payload.login_Otp, function (error, data) {
                            if (error) {
                                console.log("in otp error block");
                                res.json({
                                    code: 400,
                                    message: "Please Enter Correct Otp"
                                })
                            } else {
                                if (data.type == 'success') {
                                    let is_User_Registered = {
                                        is_User_Registered: true
                                    }
                                    try {
                                        Customers.updateOne({ mobile: loginData }, { $set: is_User_Registered }).then((result) => {
                                            if (result && result.n == 1) {
                                                console.log(result.n);
                                                res.json({
                                                    code: 200,
                                                    message: 'User Registered Successfully'
                                                })
                                            }
                                        }).catch((err) => {
                                            res.json({
                                                code: 400,
                                                message: err
                                            })
                                        })
                                    } catch (err) {
                                        console.log(err);
                                        let err = err
                                        res.json({
                                            code: 400,
                                            message: err
                                        })
                                    }

                                }
                                if (data.type == 'error') {
                                    console.log(data);
                                    console.log('OTP verification failed');
                                }
                            }
                        });
                    }
                }
                else {
                    res.json({
                        code: 204,
                        message: 'User not found'
                    })
                }
            }
        })
    } catch (err) {
        console.log(err);
        let err = err
        res.json({
            code: 400,
            message: err
        })
    }
}

customerCtrl.verificationOfRegisteredUser = (req, res) => {

    let payload = new Obj(req.body);
    console.log(payload);
    let loginMobData = {
        mobile: payload.mobile
    }
    try {
        Customers.findOne(loginMobData, (err, result) => {
            if (err) {
                res.json({
                    code: 400,
                    message: err
                })
            } else {
                if (result) {
                    console.log(result);
                    if (result.mobile == payload.mobile && result.login_Otp == payload.login_Otp) {
                        let is_User_Registered = {
                            is_User_Registered: true
                        }
                        try {
                            Customers.updateOne( loginMobData , { $set: is_User_Registered }).then((result) => {
                                if (result && result.n == 1) {
                                    console.log(result.n);
                                    res.json({
                                        code: 200,
                                        message: 'User Registered Successfully'
                                    })
                                }
                            }).catch((err) => {
                               // console.log("is",is_User_Registered);
                                res.json({
                                    code: 400,
                                    message: err
                                })
                            })
                        } catch (err) {
                            console.log(err);
                            let err = err
                            res.json({
                                code: 400,
                                message: "Enter Correct OTP"
                            })
                        } 
                    }
                }
                else {
                    res.json({
                        code: 204,
                        message: 'User not found'
                    })
                }
            }
        })
    } catch (err) {
        console.log(err);
        let err = err
        res.json({
            code: 400,
            message: err
        })
    }
}

customerCtrl.verificationOfLoginUser = (req, res,next) => {

    let payload = new Obj(req.body);
    console.log(payload);
    let loginData = {
        mobile: payload.mobile
    }
    try {
        Customers.findOne(loginData, (err, result) => {
            if (err) {
                res.json({
                    code: 400,
                    message: err
                })
            } else {
                if (result) {

                    if (result.mobile == payload.mobile && result.login_Otp == payload.login_Otp && result.is_User_Registered) {
                        
                        console.log('OTP verified successfully');
                        req.mobile=result.mobile;
                        next();

                    }
                    else{
                        console.log('OTP verification failed'); 
                    }
                }
                else {
                    res.json({
                        code: 204,
                        message: 'User not found'
                    })
                }
            }
        })
    } catch (err) {
        console.log(err);
        let err = err
        res.json({
            code: 400,
            message: err
        })
    }
}


customerCtrl.getMasterData = (req, res) => {

    let payload = new Obj(req.body);
    console.log(payload);
    let loginData = {
        mobile: payload.mobile
    }
    try {
        Customers.findOne(loginData, (err, result) => {
            if (err) {
                res.json({
                    code: 400,
                    message: err
                })
            } else {
                if (result) {

                    if (result.role == "customer" ) {
                        
                        console.log('Customer Logged In'); 
                    }
                    else{
                        console.log('Technician Logged In'); 
                    }
                    res.json({
                        code: 200,
                        message: 'User Logged In Successfully'
                    })
                }
                else {
                    res.json({
                        code: 204,
                        message: 'User not found'
                    })
                }
            }
        })
    } catch (err) {
        console.log(err);
        let err = err
        res.json({
            code: 400,
            message: err
        })
    }
}

export default customerCtrl;

