
import express from 'express';
import { default as customerApi } from './Customer.ctrl';


let router = express.Router();

//To register(Customer )
router.post('/ALLIO/api/signup', customerApi.userRegistration, customerApi.updateOtpForUserRegistration, customerApi.saveOtp);
//To verify the Registered user
router.post('/ALLIO/api/verificationOfRegisteredUser', customerApi.verificationOfRegisteredUser);

//To Login(both Customer  )
router.post('/ALLIO/api/login', customerApi.userLogin, customerApi.updateOtpForLogin, customerApi.saveOtp);

//To verify the otp( Customer )
 router.post('/ALLIO/api/verifyMobileOtp', customerApi.verificationOfLoginUser,customerApi.getMasterData);
//router.post('/ALLIO/api/verifyMobileOtp', customerApi.verificationOfLoginUser);
export default router;



