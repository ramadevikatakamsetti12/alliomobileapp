import mongoose from 'mongoose';
let schema = mongoose.Schema;

let customerSchema = new schema({

    first_Name: {
        type: String,
        required: true,
        maxlength: 50
    },
    last_Name: {
        type: String,
        maxlength: 50
    },

    email: {
        type: String,
        maxlength: 50
    },
    mobile: {
        type: String,
        required: true,
        unique: true,
    },
    address: {
        type: String,
        required: true
    },
    login_Otp:
        { type: String },

    role: {
        type: String,
        required: true
    },
    type: {
        type: String,

    },
    is_User_Registered: {
        type: Boolean,
        required: true,
        default: false
    }

})
const Customer = mongoose.model('customerNew', customerSchema);
export default Customer;